#!/usr/bin/env python

###############################################################################
#                                                                             #
#    MArVD                                                                    #
#                                                                             #
#    A python script that automates identifying potential archaeal viruses    #
#    from MetaVir (http://metavir-meb.univ-bpclermont.fr) output.             #
#                                                                             #
#    Copyright (C) Benjamin Bolduc, Dean Vik                                  #
#                                                                             #
###############################################################################
#                                                                             #
#    This library is free software; you can redistribute it and/or            #
#    modify it under the terms of the GNU Lesser General Public               #
#    License as published by the Free Software Foundation; either             #
#    version 3.0 of the License, or (at your option) any later version.       #
#                                                                             #
#    This library is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        #
#    Lesser General Public License for more details.                          #
#                                                                             #
#    You should have received a copy of the GNU Lesser General Public         #
#    License along with this library.                                         #
#                                                                             #
###############################################################################

__author__ = ["Ben Bolduc, Dean Vik"]
__copyright__ = "Copyright 2016"
__credits__ = ["Ben Bolduc", "Dean Vik"]
__license__ = "LGPLv3"
__maintainer__ = "Ben Bolduc"
__email__ = "bolduc.10@osu.edu"
__status__ = "Development"

import sys
import os
import csv
import argparse
from collections import OrderedDict

parser = argparse.ArgumentParser(description="Script that automates identifying potential archaeal viruses from MetaVir"
                                             "output.")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument("-i", "--input_fn", dest="input_fn", required=True,  # Yes, bad form. But this requires something!
                    help="MetaVir CSV file, obtained from http://metavir-meb.univ-bpclermont.fr")

inputs.add_argument("-s", "--search_fn", dest="search_fn", required=True,
                    help="File with keywords (one per line) to search for in the taxonomy in order to identify viruses."
                         " For example, 'archaeal' for archaeal viruses, 'phage' for phage(s). Can be tailored to "
                         "include any terms.")

outputs = parser.add_argument_group('Outputs')

outputs.add_argument("-o", "--output-fn", dest="output_fn", default="Putative-Archaeal-Virus", help="Output prefix.")

outputs.add_argument("-l", "--log-fn", dest="log_fn", default="MArVD.log", help="Log file name.")

filters = parser.add_argument_group('Filtering Options')

filters.add_argument("--percArchaeal", dest="percArchaeal", type=float, default=0.333,
                     help="Percent of genes required to have a match against an archaeal virus. "
                          "(optional; default: %default).")

filters.add_argument("--bitScoreNotArchaeal", dest="bitScoreNotArchaeal", type=int, default=175,
                     help="BLAST bit score for sequences with <percArchaeal (optional; default: %default).")

filters.add_argument("--percPfam", dest="percPfam", type=float, default=0.17,
                     help="Percent of genes requiring pfam match that are > bitScoreNotArchaeal. "
                          "(optional; default: %default).")

filters.add_argument("--bitScoreArchaeal", dest="bitScoreArchaeal", type=float, default=70,
                     help="BLAST bit score for sequences with >percArchaeal genes. (optional; default: %default).")

filters.add_argument("--percVStrongArchaeal", dest="percVStrongArchaeal", type=float, default=0.9,
                     help="Percent of genes required to have a highest confidence match against an archaeal virus. "
                          "(optional; default: %default).")

filters.add_argument("--percStrongArchaeal", dest="percStrongArchaeal", type=float, default=0.666,
                     help="Percent of genes required to have a high confidence match against an archaeal virus. "
                          "(optional; default: %default).")

filters.add_argument("--percWeakArchaeal", dest="percWeakArchaeal", type=float, default=0.4,
                     help="Percent of genes required to have moderate confidence match against an archaeal virus. "
                          "(optional; default: %default).")

res = parser.parse_args()


def error(msg):
    sys.stderr.write("ERROR: {}\n".format(msg))
    sys.stderr.flush()
    sys.exit(1)

try:
    with open(res.input_fn): pass
except IOError:
    error("Input file {} not found.".format(res.input_fn))

try:
    with open(res.search_fn): pass
except IOError:
    error("Search term file {} not found.".format(res.search_fn))


# Finally, where a class is actually useful
class ContigInfo(OrderedDict):
    def __init__(self, *args, **kwargs):
        super(ContigInfo, self).__init__(*args, **kwargs)

    def returnDict(self):
        return self.__dict__

contigData = {}


with open(res.input_fn, "rU") as csv_fh:

    csv_reader = csv.reader(csv_fh, delimiter=',', quotechar='"')

    headers = csv_reader.next()

    contigID = ''

    for data_lines in csv_reader:
        if not data_lines[0]:  # Does not have 'information' about contigID and is therefore annotation information

            geneID = data_lines[1]
            if geneID not in contigData[contigID]:
                # At this point, the ordering of genes does not matter, but if eventually, can just use ordereddict
                contigData[contigID][geneID] = {}

            # Skip empty contigID and now-used geneID
            contigData[contigID][geneID] = OrderedDict(zip(headers[2:], data_lines[2:]))

        else:  # Should be the contigID
            contigID = data_lines[0]  # Update contigID for this 'new' contig and its subsequent data

            if contigID not in contigData:
                contigData[contigID] = OrderedDict()

'''At this point we build a database (read: list) with user-supplied search terms. This allows the user to pick specific
terms to look for in the taxonomy that they believe (either rightly or wrongly) are indicative of their particular virus.
It also allows for the user to select a wide range of keywords that take into account spelling errors or other errors
within the taxonomy'''

searchTerms = set()  # Just in case user becomes copy-and-paste-happy
with open(res.search_fn, "rU") as searchTerm_fh:
    [searchTerms.update([line.strip()]) for line in searchTerm_fh]

# cat1= purple, cat2= green, cat3= brown

'''binnings = {
    'purple': [],
    'brown': [],
    'green': [],
    'orange': [],
    'blue': [],
    'yellow': [],
    'light_green': [],
    'pink': []
}'''

binnings = {
    'Category1': [],
    'Category3': [],
    'Category2': [],
    'orange': [],
    'blue': [],
    'yellow': [],
    'light_green': [],
    'pink': []
}

totalInputs = 0
discardedInputs = 0
binnedInputs = 0
for contig, genes in contigData.items():
    # Keep track of genes matching archaeal sequences within each contig
    archaealGenes = 0
    totalGenes = 0
    archaealPfams = 0
    phageGenes = 0
    affiliated_genes = 0
    archaealBits = []
    phageBits = []

    # Taxonomy from BLAST

    for gene, geneInfo in genes.items():

        if any(searchTerm in geneInfo['predicted taxonomy'] for searchTerm in searchTerms):
            archaealGenes += 1
            archaealBits.append(float(geneInfo['Best BLAST hit bitscore']))

            if geneInfo['PFAM affiliation']:
                archaealPfams += 1

        if 'phage' in geneInfo['predicted taxonomy']:
            phageGenes += 1
            phageBits.append(float(geneInfo['Best BLAST hit bitscore']))

        totalGenes += 1

        if geneInfo['predicted taxonomy'] != '-':
            affiliated_genes += 1

    totalInputs += 1

    # Now have total archaeal genes and total genes
    #percArchaeal = (archaealGenes + .0) / totalGenes  # len(archaealBits) SHOULD = archaealGenes
    try:
        percArchaeal = (archaealGenes + .0) / (affiliated_genes)  # len(archaealBits) SHOULD = archaealGenes
    except ZeroDivisionError:
        percArchaeal = 0

    if percArchaeal < res.percArchaeal:

        if any(bits >= res.bitScoreNotArchaeal for bits in archaealBits):  # Modified from all
            try:
                percArchaealPfams = (archaealPfams + .0) / archaealGenes

                if percArchaealPfams >= res.percPfam:
                    binnings['light_green'].append(contig)
                    binnedInputs += 1

                if percArchaealPfams < res.percPfam:
                    binnings['pink'].append(contig)
                    binnedInputs += 1

            except ZeroDivisionError:  # Less than 17%
                binnings['pink'].append(contig)
                binnedInputs += 1

        else:
            discardedInputs += 1  # Keep track of what's not used
            continue

    if percArchaeal >= res.percArchaeal:

        # Need to compare the archaeal hits with any non-archaeal, i.e. phage hits that may also be on the contig
        if any(bits >= res.bitScoreArchaeal for bits in archaealBits):

            #avgPhageBits = sum(phageBits) / float(len(phageBits))
            #avgArchaealBits = sum(archaealBits) / float(len(archaealBits))

            if any(phageBit > max(archaealBits) for phageBit in phageBits):
                if percArchaeal >= res.percWeakArchaeal:
                    if percArchaeal >= res.percStrongArchaeal:
                        binnings['Category3'].append(contig)
                        binnedInputs += 1
                    if percArchaeal < res.percStrongArchaeal:  # In order words, if it's 40%->65%
                        binnings['orange'].append(contig)
                        binnedInputs += 1

                if percArchaeal < res.percWeakArchaeal:
                    binnings['orange'].append(contig)
                    binnedInputs += 1

            # If at least 1, if not more, archaealBit scores are greater than ALL phageBit scores
            if not any(phageBit > max(archaealBits) for phageBit in phageBits):  # If at least 1, if not more, archaealBit scores are greater than ALL phageBit scores
                if not any(phageBit > max(archaealBits) for phageBit in phageBits):
                    if percArchaeal >= res.percStrongArchaeal:
                        binnings['Category1'].append(contig)
                        binnedInputs += 1
                    if percArchaeal < res.percStrongArchaeal:
                        binnings['Category2'].append(contig)
                        binnedInputs += 1

        if not any(bits >= res.bitScoreArchaeal for bits in archaealBits):  #
            if percArchaeal >= res.percVStrongArchaeal:
                binnings['blue'].append(contig)
                binnedInputs += 1

            if percArchaeal < res.percVStrongArchaeal:
                binnings['yellow'].append(contig)
                binnedInputs += 1

for bins, contigs in binnings.items():
    if not contigs:
        continue

    with open('{}-{}.csv'.format(res.output_fn, bins), 'w') as bin_fh:
        binWriter = csv.writer(bin_fh, delimiter=',', quotechar='"')
        binWriter.writerow(headers)

        for contig in contigs:

            buffer_list = ['' for x in range((len(headers) - 1))]

            binWriter.writerow([contig] + buffer_list)

            contigInfo = contigData[contig]

            for geneID, geneInfo in contigInfo.items():
                binWriter.writerow(['', geneID] + geneInfo.values())

with open(res.log_fn, 'w') as log_fh:
    log_fh.write("There were {0} input genes, {1} of which were binned and {2} excluded.".format(
        totalInputs, binnedInputs, discardedInputs) + os.linesep)
    log_fh.write("Program complete.")
