# README #

### Introduction ###

This repository is meant to facilitate access to MArVD (Metagenome Archaeal Virus Detector), published in the Vik et al. 2017 paper "Putative archaeal viruses from the mesopelagic ocean"

This is the first iteration of MArVD. MArVD in its current state takes as an input the detailed ORF annotations file from [MetaVir II](http://metavir-meb.univ-bpclermont.fr/), as well as a list of archaeal virus keywords which is provided in this repository.

The output is multiple categories, summarized in the manuscript, containing lists of detailed ORF annotations in the same format as the input MetaVir II containing only those contigs which satisfy the thresholds detailed in the manuscript.

### Installation ###

*  Python 2.7 or greater

### Usage ###

```bash
python MarVD.py -i your_annotation_file -s keyword_file --percArchaeal 0.xx --bitScoreNotArchaeal xx --percPfam 0.xx --bitScoreArchaeal xx --percVStrongArchaeal 0.xx --percStrongArchaeal 0.xx --percWeakArchaeal 0.xx > out
```

Existing options and help can be viewed by calling "--help" Options here are presented in both long-form and abbreviated forms.

#### Required ####

**-i** / **--input_fn**: Input detailed ORF annotation file from MetaVir II.

**-s** / **--search_fn**: Text file with archaeal virus keyword list, one word per line.


#### Optional ####

**--percArchaeal**: Percent of genes required to have a match against an archaeal virus. default = 0.33

**--bitScoreNotArchaeal**: BLAST bit score for sequences. default = 175

**--percPfam**: Percent of genes requiring Pfam match that are > bitScoreNotArchaeal. default = 0.17

**--bitScoreArchaeal**: BLAST bit score for sequences with >percArchaeal genes. default = 70

**--percVStrongArchaeal**: Percent of genes required to have a highest confidence match against an archaeal virus. default = 0.9

**--percStrongArchaeal**: Percent of genes required to have a high confidence match against an archaeal virus. default = 0.666

**--percWeakArchaeal**: Percent of genes required to have a high confidence match against an archaeal virus. default = 0.4

### Outputs

**-o** / **--output-fn**: Output prefix. Default: "Putative_Archaeal_Virus"

**-l** / **--log-fn**: Log file name. Default: "AVIFM.log"

### Citation ###

If you find MArVD useful in your research, please cite:



For questions please contact Dean Vik at vik.1@osu.edu
